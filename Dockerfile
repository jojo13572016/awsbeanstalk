FROM golang:1.5.1
MAINTAINER Bean <jojo13572001@yahoo.com.tw>
ENTRYPOINT ["bash", "/etc/my_init.d/boot.sh"]

ARG APP_NAME

# set golang env
ENV GOPATH /go

# set environment variables
ENV app_name=example

# add new use app
#RUN useradd app

#add known_hosts
#RUN mkdir -p /go/src/.ssh && touch /root/.ssh/known_hosts 

# add boot script
ADD boot.sh /etc/my_init.d/boot.sh

# setup logrotate
ADD logrotate /etc/logrotate.d/${app_name}

# add deploy keys
ADD .ssh/id_rsa /root/.ssh/id_rsa
ADD .ssh/id_rsa.pub /root/.ssh/id_rsa.pub
#RUN chmod 600 /home/app/.ssh/id_rsa &&\
# chmod 644 /home/app/.ssh/id_rsa.pub &&\
# chown -R app:app /home/app/.ssh
RUN echo $(ssh-keyscan -H github.com) >> /root/.ssh/known_hosts &&\
  echo $(ssh-keyscan -H bitbucket.org) >> /root/.ssh/known_hosts &&\
  echo $(ssh-keyscan -H gitlab.com) >> /root/.ssh/known_hosts

# setup beego
RUN go get github.com/astaxie/beego
RUN go get github.com/beego/bee

# setup nginx
#RUN rm -f /etc/service/nginx/down &&\
#  rm /etc/nginx/sites-enabled/default
#ADD nginx.conf /etc/nginx/sites-enabled/${app_name}.conf
#ADD nginx_env.conf /etc/nginx/main.d/env.conf

#RUN chmod 777 /etc/my_init.d/boot.sh
#RUN sh /etc/my_init.d/boot.sh
EXPOSE 3000

# clean up when done
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

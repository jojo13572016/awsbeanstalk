#!/bin/bash
APP_NAME=example
MAIN_CONTAINER=production
DB_CONTAINER=${APP_NAME}_mysql
DB_PASSWORD=password
DEFAULT_DB_NAME=backenddb

echo "Trying to cleanup any leftover containers"
#docker stop $DB_CONTAINER && docker rm -v $DB_CONTAINER
docker stop $MAIN_CONTAINER && docker rm -f $MAIN_CONTAINER

#echo "Starting MySQL"
#docker run --name $DB_CONTAINER -e MYSQL_ROOT_PASSWORD="$DB_PASSWORD" -e MYSQL_DATABASE="$DEFAULT_DB_NAME" -d mysql:5.7



docker build -t jojo13572001/$MAIN_CONTAINER --build-arg APP_NAME=$APP_NAME . 
#docker run --name $MAIN_CONTAINER --rm -v $(pwd):/go/src/$APP_NAME -w /go/src/$APP_NAME $APP_NAME bee new server 
#docker run --name $MAIN_CONTAINER -it -p 3000:8080 jojo13572001/$MAIN_CONTAINER bash /etc/my_init.d/boot.sh
docker run -d -P jojo13572001/$MAIN_CONTAINER
